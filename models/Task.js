const mongoose = require("mongoose");

const taskSchema = mongoose.Schema({
    name: {
        type: String,
        required: [true, "Task is required!"]
    },
    status: {
        type: String,
        default: "pending"
    }
});


//exporting files to other page
module.exports = mongoose.model("Task", taskSchema);