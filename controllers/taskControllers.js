const Task = require("../models/Task");

module.exports.createTask = (req, res) => {
    console.log(req.body);
    Task.findOne({ name: req.body.name }).then(result => {
            console.log(result)
            if (result != null && result.name == req.body.name) {
                return res.send("Duplicate Task");
            } else {
                newTask = new Task({
                    name: req.body.name,
                    status: req.body.status
                });
                newTask.save()
                    .then(result => res.send(result))
                    .catch(error => res.send(error));
            }
        })
        .catch(error => res.send(error))
};

module.exports.getAllTasksController = (req, res) => {
    Task.find({})
        .then(task => res.send(task))
        .catch(error => res.send(error));
};

module.exports.getSingleTaskController = (req, res) => {
    console.log(req.params);
    Task.findById(req.params.taskID)
        .then(result => res.send(result))
        .catch(error => res.send(error));
};

module.exports.updateTaskStatusController = (req, res) => {
    console.log(req.params.taskID);
    console.log(req.body);

    let updates = {
        status: req.body.status
    };
    Task.findByIdAndUpdate(req.params.taskID, updates, { new: true })
        .then(updatedTask => res.send(updatedTask))
        .catch(error => res.send(error));
};

module.exports.deleteTaskController = (req, res) => {
    console.log(req.params.taskID);

    Task.findByIdAndDelete(req.params.taskID)
        .then(removedTask => res.send(removedTask))
        .catch(error => res.send(error));
};