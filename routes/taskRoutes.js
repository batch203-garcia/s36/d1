const express = require("express");

//allows access to http methods
const router = express.Router();

const taskControllers = require("../controllers/taskControllers");

console.log(taskControllers);

//Create Task Routes
router.post("/", taskControllers.createTask);

//view all tasks route
router.get("/allTasks", taskControllers.getAllTasksController);

//view single task
router.get("/getSingleTask/:taskID", taskControllers.getSingleTaskController);

//update task -> status
router.patch("/updateTask/:taskID", taskControllers.updateTaskStatusController);

//delete
router.delete("/deleteTask/:taskID", taskControllers.deleteTaskController);

//used in the server
module.exports = router;