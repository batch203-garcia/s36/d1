//require modules
const express = require("express");
const mongoose = require("mongoose");

//port
const port = 4000;

//server
const app = express();

//mongoDB connection 
mongoose.connect("mongodb+srv://keanfinity:kean.zuitt@wdc028-course-booking.52n84dy.mongodb.net/WDC028-Course-Booking?retryWrites=true&w=majority", {
    useNewUrlParser: true,
    useUnifiedTopology: true
});

//connection check
let db = mongoose.connection;
//if con error
db.on("error", console.error.bind(console, "connection error"));
//if con okay
db.once("open", () => console.log("Cloud DB Connection - OK"));

//middlewares
app.use(express.json());
app.use(express.urlencoded({ extended: true }));

//Routes Grouping
const taskRoutes = require("./routes/taskRoutes");
app.use("/tasks", taskRoutes);

//port listener
app.listen(port, () => console.log(`SERVER IS RUNNING AT PORT: ${port}`));